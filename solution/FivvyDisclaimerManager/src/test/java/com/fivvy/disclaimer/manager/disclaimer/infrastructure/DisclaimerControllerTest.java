package com.fivvy.disclaimer.manager.disclaimer.infrastructure;


import com.fivvy.disclaimer.manager.disclaimer.domain.Disclaimer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DisclaimerControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DisclaimerRepository disclaimerRepository;

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        disclaimerRepository.deleteAll();
        Disclaimer disclaimer1 = new Disclaimer("ID-1", "Disclaimer 1", "Text: Cold - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        Disclaimer disclaimer2 = new Disclaimer("ID-2", "Disclaimer 2", "Text: Hot - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        disclaimerRepository.save(disclaimer1);
        disclaimerRepository.save(disclaimer2);
    }

    @Test
    public void testListDisclaimers() {
        ResponseEntity<List<Disclaimer>> response = restTemplate.exchange(
                "http://localhost:" + port + "/disclaimers",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Disclaimer>>() {}
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().size());
    }

    @Test
    public void testGetDisclaimerById() {
        Disclaimer disclaimer = disclaimerRepository.findAll().get(0);

        ResponseEntity<Disclaimer> response = restTemplate.getForEntity(
                "http://localhost:" + port + "/disclaimers/" + disclaimer.getId(),
                Disclaimer.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(disclaimer.getId(), response.getBody().getId());
        assertEquals(disclaimer.getName(), response.getBody().getName());
        assertEquals(disclaimer.getText(), response.getBody().getText());
        assertEquals(disclaimer.getVersion(), response.getBody().getVersion());
    }

    @Test
    public void testCreateDisclaimer() {
        Disclaimer disclaimer = new Disclaimer("ID-new", "Disclaimer new", "Text: Warm - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());

        ResponseEntity<Disclaimer> response = restTemplate.postForEntity(
                "http://localhost:" + port + "/disclaimers",
                disclaimer,
                Disclaimer.class
        );

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody().getId());
        assertEquals(disclaimer.getName(), response.getBody().getName());
        assertEquals(disclaimer.getText(), response.getBody().getText());
        assertEquals(disclaimer.getVersion(), response.getBody().getVersion());

        List<Disclaimer> disclaimers = disclaimerRepository.findAll();
        assertEquals(3, disclaimers.size());
    }

    @Test
    public void testUpdateDisclaimer() {
        Disclaimer disclaimer = disclaimerRepository.findAll().get(0);
        disclaimer.withName("Updated Name");

        restTemplate.put(
                "http://localhost:" + port + "/disclaimers/" + disclaimer.getId(),
                disclaimer
        );

        ResponseEntity<Disclaimer> response = restTemplate.getForEntity(
                "http://localhost:" + port + "/disclaimers/" + disclaimer.getId(),
                Disclaimer.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(disclaimer.getId(), response.getBody().getId());
        assertEquals(disclaimer.getName(), response.getBody().getName());
        assertEquals(disclaimer.getText(), response.getBody().getText());
        assertEquals(disclaimer.getVersion() + 1, response.getBody().getVersion().longValue());
        assertEquals(disclaimer.getCreateAt(), response.getBody().getCreateAt());
        assertNotEquals(disclaimer.getUpdateAt(), response.getBody().getUpdateAt());
    }

    @Test
    public void testDeleteDisclaimer() {
        Disclaimer disclaimer = disclaimerRepository.findAll().get(0);

        restTemplate.delete("http://localhost:" + port + "/disclaimers/" + disclaimer.getId());

        assertFalse(disclaimerRepository.findById(disclaimer.getId()).isPresent());
    }

}
