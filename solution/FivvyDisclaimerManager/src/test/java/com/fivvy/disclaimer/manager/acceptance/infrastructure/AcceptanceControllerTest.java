package com.fivvy.disclaimer.manager.acceptance.infrastructure;

import com.fivvy.disclaimer.manager.acceptance.domain.Acceptance;
import com.fivvy.disclaimer.manager.disclaimer.domain.Disclaimer;
import com.fivvy.disclaimer.manager.disclaimer.infrastructure.DisclaimerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AcceptanceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AcceptanceRepository acceptanceRepository;

    @Autowired
    private DisclaimerRepository disclaimerRepository;

    @LocalServerPort
    private int port;

    private Disclaimer disclaimer;

    @Before
    public void setUp() {
        disclaimerRepository.deleteAll();
        acceptanceRepository.deleteAll();
        Disclaimer disclaimer1 = new Disclaimer("ID-1", "Disclaimer 1", "Text: Cold - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        Disclaimer disclaimer2 = new Disclaimer("ID-2", "Disclaimer 2", "Text: Hot - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        Disclaimer disclaimer3 = new Disclaimer("ID-3", "Disclaimer 3", "Text: Warm - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        Disclaimer disclaimer4 = new Disclaimer("ID-4", "Disclaimer 4", "Text: Warm - Neutral", 1l, LocalDateTime.now(), LocalDateTime.now());
        disclaimerRepository.save(disclaimer1);
        disclaimerRepository.save(disclaimer2);
        disclaimerRepository.save(disclaimer3);
        disclaimerRepository.save(disclaimer4);
        Acceptance acceptance1 = new Acceptance("ID-1", "User 1", LocalDateTime.now());
        Acceptance acceptance2 = new Acceptance("ID-2", "User 1", LocalDateTime.now());
        Acceptance acceptance3 = new Acceptance("ID-3", "User 2", LocalDateTime.now());
        acceptanceRepository.save(acceptance1);
        acceptanceRepository.save(acceptance2);
        acceptanceRepository.save(acceptance3);
    }

    @Test
    public void testListAcceptances() {
        ResponseEntity<List<Acceptance>> response = restTemplate.exchange(
                "http://localhost:" + port + "/acceptances",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Acceptance>>() {}
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, response.getBody().size());
    }

    @Test
    public void testListAcceptancesByUserId() {
        ResponseEntity<List<Acceptance>> response = restTemplate.exchange(
                "http://localhost:" + port + "/acceptances?userId=User 1",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Acceptance>>() {}
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().size());
        assertEquals("User 1", response.getBody().get(0).getUserId());
        assertEquals("User 1", response.getBody().get(1).getUserId());
    }

    @Test
    public void testGetAcceptanceById() {
        Acceptance acceptance = acceptanceRepository.findAll().get(0);

        ResponseEntity<Acceptance> response = restTemplate.getForEntity(
                "http://localhost:" + port + "/acceptances/" + acceptance.getDisclaimerId(),
                Acceptance.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(acceptance.getDisclaimerId(), response.getBody().getDisclaimerId());
        assertEquals(acceptance.getUserId(), response.getBody().getUserId());
    }

    @Test
    public void testCreateAcceptance() {
        Acceptance acceptance = new Acceptance("ID-2", "User New", LocalDateTime.now());

        ResponseEntity<Acceptance> response = restTemplate.postForEntity(
                "http://localhost:" + port + "/acceptances",
                acceptance,
                Acceptance.class
        );

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody().getDisclaimerId());
        assertEquals("ID-2", response.getBody().getDisclaimerId());
        assertEquals(acceptance.getUserId(), response.getBody().getUserId());

        List<Acceptance> acceptances = acceptanceRepository.findByUserId("User New");
        assertEquals(1, acceptances.size());
    }

    @Test
    public void testDeleteAcceptance() {
        Acceptance acceptance = acceptanceRepository.findAll().get(0);

        restTemplate.delete("http://localhost:" + port + "/acceptances/" + acceptance.getDisclaimerId());

        assertFalse(acceptanceRepository.findById(acceptance.getDisclaimerId()).isPresent());
    }

}
