package com.fivvy.disclaimer.manager.disclaimer.application;

import com.fivvy.disclaimer.manager.disclaimer.domain.Disclaimer;
import com.fivvy.disclaimer.manager.disclaimer.domain.DisclaimerUpdateRequest;
import com.fivvy.disclaimer.manager.disclaimer.infrastructure.DisclaimerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisclaimerService {
    @Autowired
    private DisclaimerRepository disclaimerRepository;

    public List<Disclaimer> findByTextContaining(String searchText) {
        return disclaimerRepository.findByTextContaining(searchText);
    }

    public List<Disclaimer> findAll() {
        return disclaimerRepository.findAll();
    }

    public Optional<Disclaimer> find(String id) {
        return disclaimerRepository.findById(id);
    }

    public Optional<Disclaimer> save(Disclaimer disclaimer) {
        return Optional.of(disclaimerRepository.save(disclaimer));
    }

    public void delete(String id) {
        disclaimerRepository.deleteById(id);
    }
    public Optional<Disclaimer> update(String id, DisclaimerUpdateRequest updatedDisclaimer) {
        return this
                .find(id)
                .map(disclaimer ->
                        disclaimerRepository.save(
                                disclaimer
                                        .withName(updatedDisclaimer.getName())
                                        .withText(updatedDisclaimer.getText())
                                        .registerUpdate()
                        )
                );
    }
}
