package com.fivvy.disclaimer.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisclaimerManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DisclaimerManagerApplication.class, args);
    }
}
