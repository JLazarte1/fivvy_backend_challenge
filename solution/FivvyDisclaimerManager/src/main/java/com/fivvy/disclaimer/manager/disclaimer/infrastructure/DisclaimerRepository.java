package com.fivvy.disclaimer.manager.disclaimer.infrastructure;

import com.fivvy.disclaimer.manager.disclaimer.domain.Disclaimer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisclaimerRepository extends JpaRepository<Disclaimer, String> {
    List<Disclaimer> findByTextContaining(String searchText);
}

