package com.fivvy.disclaimer.manager.acceptance.infrastructure;

import com.fivvy.disclaimer.manager.acceptance.domain.Acceptance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcceptanceRepository extends JpaRepository<Acceptance, String> {
    List<Acceptance> findByUserId(String userId);
}

