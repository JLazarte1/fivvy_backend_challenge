package com.fivvy.disclaimer.manager.disclaimer.infrastructure;

import com.fivvy.disclaimer.manager.disclaimer.domain.Disclaimer;
import com.fivvy.disclaimer.manager.disclaimer.domain.DisclaimerUpdateRequest;
import com.fivvy.disclaimer.manager.disclaimer.application.DisclaimerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/disclaimers")
public class DisclaimerController {
    @Autowired
    private DisclaimerService disclaimerService;

    @GetMapping("")
    public List<Disclaimer> getDisclaimers(@RequestParam(required = false) String searchText) {
        return (searchText == null || searchText.isBlank()) ?
                disclaimerService.findAll() :
                disclaimerService.findByTextContaining(searchText);
    }

    @GetMapping("/{id}")
    public Disclaimer getDisclaimer(@PathVariable String id) {
        return disclaimerService
                .find(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public Disclaimer updateDisclaimer(@PathVariable String id, @RequestBody DisclaimerUpdateRequest updatedDisclaimer) {
        return disclaimerService
                .update(id, updatedDisclaimer)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public void deleteDisclaimer(@PathVariable String id) {
        disclaimerService.delete(id);
    }

    @PostMapping("")
    public ResponseEntity<?> createDisclaimer(@RequestBody Disclaimer disclaimer, HttpServletRequest request) {
        return disclaimerService
                .save(disclaimer)
                .map(saved ->
                        ResponseEntity
                                .created(
                                        URI.create(request.getRequestURI() + "/" + saved.getId())
                                )
                                .body(saved)
                ).orElse(ResponseEntity.badRequest().build());
    }
}
