package com.fivvy.disclaimer.manager.acceptance.infrastructure;

import com.fivvy.disclaimer.manager.acceptance.application.AcceptanceService;
import com.fivvy.disclaimer.manager.acceptance.domain.Acceptance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/acceptances")
public class AcceptanceController {

    @Autowired
    private AcceptanceService acceptanceService;

    @GetMapping
    public List<Acceptance> getAll(@RequestParam(required = false) String userId) {
            return (userId == null) ?
                    acceptanceService.findAll() :
                    acceptanceService.findByUserId(userId);
    }

    @GetMapping("/{disclaimerId}")
    public Acceptance getByDisclaimerId(@PathVariable String disclaimerId) {
        return acceptanceService
                .findById(disclaimerId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping()
    public ResponseEntity<?> createAcceptance(@RequestBody Acceptance acceptance, HttpServletRequest request) {
        return acceptanceService
                .save(acceptance)
                .map(saved ->
                        ResponseEntity
                                .created(
                                        URI.create(request.getRequestURI() + "/" + saved.getDisclaimerId())
                                ).body(saved)
                ).orElse(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/{disclaimerId}")
    public void deleteAcceptance(@PathVariable String disclaimerId) {
        acceptanceService.delete(disclaimerId);
    }
}
