package com.fivvy.disclaimer.manager.acceptance.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Acceptance {
    @Id
    private @NonNull String disclaimerId;
    private @NonNull String userId;
    private @NonNull LocalDateTime createAt = LocalDateTime.now();
}