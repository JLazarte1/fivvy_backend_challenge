package com.fivvy.disclaimer.manager.acceptance.application;

import com.fivvy.disclaimer.manager.acceptance.domain.Acceptance;
import com.fivvy.disclaimer.manager.acceptance.infrastructure.AcceptanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AcceptanceService {
    @Autowired
    private AcceptanceRepository acceptanceRepository;

    public List<Acceptance> findAll() {
        return acceptanceRepository.findAll();
    }

    public List<Acceptance> findByUserId(String userId) {
        return acceptanceRepository.findByUserId(userId);
    }

    public Optional<Acceptance> findById(String disclaimerId) {
        return acceptanceRepository.findById(disclaimerId);
    }

    public Optional<Acceptance> save(Acceptance acceptance) {
        return Optional.of(acceptanceRepository.save(acceptance));
    }

    public void delete(String disclaimerId) {
        acceptanceRepository.deleteById(disclaimerId);
    }
}
