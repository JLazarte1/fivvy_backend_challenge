package com.fivvy.disclaimer.manager.disclaimer.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DisclaimerUpdateRequest {
    private String name;
    private String text;
}