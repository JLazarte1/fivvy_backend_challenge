package com.fivvy.disclaimer.manager.disclaimer.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.With;
import lombok.AccessLevel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Disclaimer {
    @Id
    private String id = UUID.randomUUID().toString();
    @With private @NonNull String name;
    @With private @NonNull String text;
    @With(AccessLevel.PROTECTED) private @NonNull Long version = 0L;
    private LocalDateTime createAt = LocalDateTime.now();
    @With(AccessLevel.PROTECTED) private @NonNull LocalDateTime updateAt = LocalDateTime.now();

    public Disclaimer registerUpdate() {
        return this
                .withVersion(this.getVersion() + 1)
                .withUpdateAt(LocalDateTime.now());
    }

}